# NumeroReal



## Questão 1 – 5 pontos

Escreva uma classe Java que represente um número real. Um bom nome para essa classe é
NumeroReal. Ok, sabemos que existe a classe Double na API, mas o objetivo aqui é avaliar a
aplicação dos pilares de OO. Portanto, finja que a classe Double não existe. A classe
NumeroReal deve incluir:

a) Construtor que recebem como argumento um valor do tipo double;
b) Métodos getter e setter;
c) Método de soma que recebe como argumento outra instância de NumeroReal e retorna
uma instância de NumeroReal com o resultado da soma;
d) Sobrescrita do método toString.
Aplicando o princípio de herança, escreva a classe NumeroComplexo como subclasse de
NumeroReal. Lembre-se que números complexos são formados por uma parte real (x) e uma
parte imaginária (y), sendo escritos como x + y i, onde i é a unidade imaginária que satisfaz a
equação i2 = -1. Logo, é preciso incluir a parte imaginária nesta classe e realizar as seguintes
implementações:

e) Construtor que recebem como argumento dois valores do tipo double, sendo um para a
parte real e outro para a parte imaginária;
f) Métodos getter e setter;
g) Método somar que recebe como argumento outra instância de NumeroComplexo e
retorna uma instância de NumeroComplexo com o resultado da soma;
h) Sobrescrita do método toString.
Dica: Na soma dos números complexos z1 = a + b i e z2 = c + d i o resultado é r = (a + c) + (b + d) i.
