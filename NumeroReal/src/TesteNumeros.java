public class TesteNumeros {

    public static void main(String[] args) {
        // Teste para NumeroReal
        NumeroReal numeroReal1 = new NumeroReal(5.5);
        NumeroReal numeroReal2 = new NumeroReal(3.7);

        System.out.println("Número Real 1: " + numeroReal1.toString());
        System.out.println("Número Real 2: " + numeroReal2.toString());

        // Soma de números reais
        NumeroReal resultadoSomaReal = numeroReal1.soma(numeroReal2);
        System.out.println("Soma de Números Reais: " + resultadoSomaReal.toString());

        // Teste para NumeroComplexo
        NumeroComplexo numeroComplexo1 = new NumeroComplexo(2.0, 4.0);
        NumeroComplexo numeroComplexo2 = new NumeroComplexo(1.5, 2.5);

        System.out.println("\nNúmero Complexo 1: " + numeroComplexo1.toString());
        System.out.println("Número Complexo 2: " + numeroComplexo2.toString());

        // Soma de números complexos
        NumeroComplexo resultadoSomaComplexo = numeroComplexo1.soma(numeroComplexo2);
        System.out.println("Soma de Números Complexos: " + resultadoSomaComplexo.toString());
    }
}
