public class NumeroComplexo extends NumeroReal {

    private double parteImaginaria;

    public NumeroComplexo(double parteReal, double parteImaginaria) { 
    //Item (e)
        super(parteReal);
        this.parteImaginaria = parteImaginaria;
    }

    public double getParteImaginaria() { // Item (f)
        return parteImaginaria;
    }

    public void setParteImaginaria(double parteImaginaria) { // Item (f)
        this.parteImaginaria = parteImaginaria;
    }

    public NumeroComplexo soma(NumeroComplexo outro) { // Item (g)
        return new NumeroComplexo(
                parteReal + outro.parteReal,
                parteImaginaria + outro.parteImaginaria);
    }

    public String toString() { // Item (h)
        return String.format("Parte Real: %1.4f; Parte Imaginária: %1.4f",
                parteReal, parteImaginaria);
    }
}


