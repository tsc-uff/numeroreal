public class NumeroReal {
    protected double parteReal;

    public NumeroReal(double parteReal) { // Item (a)
        this.parteReal = parteReal;
    }

    public double getParteReal() { // Item (b)
        return parteReal;
    }

    public void setParteReal(double parteReal) { // Item (b)
        this.parteReal = parteReal;
    }

    public NumeroReal soma(NumeroReal outro) { // Item (c)
        return new NumeroReal(parteReal + outro.parteReal);
    }

    public String toString() { // Item (d)
        return String.format("Valor: %1.4f", parteReal);
    }
}